class AirportOpeningTimesController < ApplicationController
  def new
  end

  def create
    notams_txt = params[:notams].read
    notams = NotamSelector.new(notams_txt).select
    @notams = notams.map { |notam| NotamParser.new(notam) }
    @notams.each { |notam| notam.parse! }
  end
end
