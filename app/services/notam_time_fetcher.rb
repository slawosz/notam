# Class that turns string containing days, ie:
# "MON-THU 0530-2115, FRI 0530-2000,   SAT 0730-1600, SUN 1000-2115."
# to array that contains 7 elements. Each element are opening times,
# for every day of week, starting from monday
class NotamTimeFetcher
  DAYS = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
  DAYS_REGEXP = Regexp.new(DAYS.join("|"))

  def initialize(txt)
    @txt = txt
  end

  def fetch!
    # remove commas, dots and more than one space
    @txt = @txt.gsub(/,|\.|:/, "").gsub(/\s+/, " ")
    # if letter is next to digit, add a space there -
    # this is format of data we expect
    @txt = @txt.gsub(/([^0-9\-])(\d)/, '\1 \2')

    prepare_days_hash
    extract_days
  end

  private

  def prepare_days_hash
    @days_hash = Hash.new { |k,v| k[v] = []}
    current_period = nil
    txt_array = @txt.split(" ")
    txt_array.each do |elem|
      if elem.match(DAYS_REGEXP)
        current_period = elem
      else
        @days_hash[current_period] << elem
      end
    end
  end

  def extract_days
    extracted_days = []
    @days_hash.each do |days_period, values|
      # if its single day?
      if DAYS.include? days_period
        extracted_days << values.join(" ")
      elsif days_period =~ /\w{3}-\w{3}/
        from, to = days_period.split("-")
        # create integer range to iterate over based on period days
        (DAYS.index(from)..DAYS.index(to)).each do |index|
          extracted_days << values.join(" ")
        end
      end
    end
    extracted_days
  end
end
