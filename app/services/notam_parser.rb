# NotamParser extract important informations from NOTAM (Notice to Airman)
# Please call `parse!` before extracting informations
class NotamParser
  OPENING_HOURS_TXT = /AERODROME HOURS OF OPS\/SERVICE/
  attr_reader :a_section, :e_section

  def initialize(notam)
    @notam = notam
  end

  def parse!
    lines = @notam.split("\n")
    lines = lines[0..-3] # I am not interested in last 2 lines now

    e_section_started = false
    e_section = []
    lines.each do |line|
      if e_section_started
        e_section << line
      end
      if line =~ /A\)/
        parse_a_section(line)
      end
      if line =~ /E\)/
        e_section_started = true
        e_section << line.gsub(/E\)\s?/, "")
      end
    end
    parse_e_section(e_section)
  end

  # TODO: this is beyond task scope, but there should be validation
  # that only e sections containing OPENING_HOURS_TXT should be used to
  # extract opening times, othervise it will return misleading info
  def opening_times
    NotamTimeFetcher.new(e_section.gsub(OPENING_HOURS_TXT, "")).fetch!
  end

  private

  def parse_a_section(line)
    line = line.gsub(" ", "")
    /A\)(?<icao>\w{4})/ =~ line
    @a_section = icao
  end

  def parse_e_section(lines)
    @e_section = lines.join(" ")
  end
end
