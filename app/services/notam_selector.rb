class NotamSelector
  REQUIRED = /AERODROME HOURS OF OPS\/SERVICE/

  def initialize(txt)
    @txt = txt.gsub("\r\n", "\n")
  end

  def select
    @selected ||= @txt.split("\n\n").select { |notam| notam =~ REQUIRED }
  end
end
