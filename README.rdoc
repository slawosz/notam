== NOTAM TASK

To setup, you may need:
* `bundle install`
* `rake db:migrate` (there is no db, but its standard rails app)

To run specs, do
`rspec spec`

To run server, do
`rails s`

Go to http://localhost:3000 or to secure-plateau-76930.herokuapp.com if you want to see it life.


