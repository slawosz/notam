Rails.application.routes.draw do
  resources :airport_opening_times, only: [:new, :create]
  root to: "airport_opening_times#new"
end
