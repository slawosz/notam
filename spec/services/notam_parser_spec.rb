require 'rails_helper'

describe NotamParser do
  shared_context "notam parser" do
    let(:notam_parser) do
      NotamParser.new(notam)
    end

    before do
      notam_parser.parse!
    end

    specify "A section" do
      expect(notam_parser.a_section).to eq(a_section)
    end

    specify "E section" do
      expect(notam_parser.e_section).to eq(e_section)
    end

    specify "opening times" do
      expect(notam_parser.opening_times).to eq(opening_times)
    end
  end

  describe "standard notam" do
    let(:notam) do
      <<-TXT
B0519/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ B) 1502271138 C) 1503012359
E) AERODROME HOURS OF OPS/SERVICE MON-WED 0500-1830 THU 0500-2130
FRI
0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED
CREATED: 27 Feb 2015 11:40:00
SOURCE: EUECYIYN
TXT
    end

    let(:a_section) do
      "ESGJ"
    end

    let(:e_section) do
      "AERODROME HOURS OF OPS/SERVICE MON-WED 0500-1830 THU 0500-2130 FRI 0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED"
    end

    let(:opening_times) do
      ["0500-1830", "0500-1830", "0500-1830", "0500-2130", "0730-2100", "0630-0730 1900-2100", "CLOSED"]
    end

    it_should_behave_like "notam parser"
  end

  describe "standard notam without new line at the end" do
    let(:notam) do
      notam = <<-TXT
B0519/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5746N01404E005
A) ESGJ B) 1502271138 C) 1503012359
E) AERODROME HOURS OF OPS/SERVICE MON-WED 0500-1830 THU 0500-2130
FRI
0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED
CREATED: 27 Feb 2015 11:40:00
SOURCE: EUECYIYN
TXT
      notam.chomp
    end

    let(:a_section) do
      "ESGJ"
    end

    let(:e_section) do
      "AERODROME HOURS OF OPS/SERVICE MON-WED 0500-1830 THU 0500-2130 FRI 0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED"
    end

    let(:opening_times) do
      ["0500-1830", "0500-1830", "0500-1830", "0500-2130", "0730-2100", "0630-0730 1900-2100", "CLOSED"]
    end

    it_should_behave_like "notam parser"
  end

  describe "notam example 2" do
    let(:notam) do
      <<-TXT
B0296/15 NOTAMN
Q) ESAA/QFAAH/IV/NBO/A /000/999/5740N01821E005
A) ESSV B) 1503130000 C) 1503292359
E) AERODROME HOURS OF OPS/SERVICE. MON-THU 0530-2115, FRI 0530-2000,
  SAT 0730-1600, SUN 1000-2115.
  CREATED: 02 Feb 2015 12:20:00
SOURCE: EUECYIYN
TXT
    end

    let(:a_section) do
      "ESSV"
    end

    let(:e_section) do
      "AERODROME HOURS OF OPS/SERVICE. MON-THU 0530-2115, FRI 0530-2000,   SAT 0730-1600, SUN 1000-2115."
    end

    let(:opening_times) do
      ["0530-2115", "0530-2115", "0530-2115", "0530-2115", "0530-2000", "0730-1600", "1000-2115"]
    end

    it_should_behave_like "notam parser"
  end
end
