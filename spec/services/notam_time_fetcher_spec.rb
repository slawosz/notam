require 'rails_helper'

describe NotamTimeFetcher do
  shared_examples_for "time fetcher" do
    specify "correct time is fetched" do
      expect(NotamTimeFetcher.new(e_section).fetch!).to eq(result)
    end
  end

  describe "example 1" do
    let(:e_section) { "MON-THU 0530-2115, FRI 0530-2000,   SAT 0730-1600, SUN 1000-2115." }

    let(:result) do
      ["0530-2115", "0530-2115", "0530-2115", "0530-2115", "0530-2000", "0730-1600", "1000-2115"]
    end

    it_should_behave_like "time fetcher"
  end

  describe "example 2" do
    let(:e_section) { "MON-WED 0500-1830 THU 0500-2130 FRI 0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED" }

    let(:result) do
      ["0500-1830", "0500-1830", "0500-1830", "0500-2130", "0730-2100", "0630-0730 1900-2100", "CLOSED"]
    end

    it_should_behave_like "time fetcher"
  end

  describe "colons are not welcome" do
    let(:e_section) { ":MON-WED 0500-1830 THU 0500-2130 FRI 0730-2100 SAT 0630-0730, 1900-2100 SUN CLOSED" }

    let(:result) do
      ["0500-1830", "0500-1830", "0500-1830", "0500-2130", "0730-2100", "0630-0730 1900-2100", "CLOSED"]
    end

    it_should_behave_like "time fetcher"
  end

  describe "some data are not properly spaced" do
    let(:e_section) { "MON-WED0500-1830 THU0500-2130 FRI 0730-2100 SAT0630-0730, 1900-2100 SUN CLOSED" }

    let(:result) do
      ["0500-1830", "0500-1830", "0500-1830", "0500-2130", "0730-2100", "0630-0730 1900-2100", "CLOSED"]
    end

    it_should_behave_like "time fetcher"
  end
end
