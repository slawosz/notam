require 'rails_helper'

describe "Notam App" do

  it "parses uploaded notam" do
    visit '/airport_opening_times/new'

    attach_file('notams', "#{Rails.root}/spec/fixtures/notam_ok.txt")

    click_button 'Process'

    expect(page).to have_content 'ESGJ'
    expect(page).to have_content '0500-1830'
  end
end
